package model;

import java.util.ArrayList;

public class User {
	private String name;
	private String id;
	private ArrayList<Book> books = new ArrayList<Book>();
	
	public User(String name,String id){
		this.name = name;
		this.id = id;
	}
	
	public void borrowBook(Book book){
		books.add(book);
	}
}
