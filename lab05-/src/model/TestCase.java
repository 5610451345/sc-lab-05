package model;

import model.Book;
import model.Libary;
import model.ReferencesBook;
import model.User;

public class TestCase {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new TestCase();

	}
	
	public TestCase(){
		testCase();
	}
	
	private void testCase(){
		Libary lib = new Libary();
		User student = new User("Vachiravit","5610450331");
		Book book1 = new Book("Java","Sutthirat");
		Book book2 = new Book("Python","Vachu");
		Book book3 = new Book("Vb.net","Rip in pepperino");
		System.out.println("< add 3 books >");
		ReferencesBook refBook = new ReferencesBook("News", "Matichon");
		lib.addBook(book1); lib.addBook(book2); lib.addBook(book3);
		lib.addRefBook(refBook);
		System.out.println("Books count: "+lib.getBookCount());
		System.out.println("show all books: "+lib.showBooks());
		System.out.println("reference books count: "+lib.getRefBookCount());
		System.out.println("< Borrowed Java >");
		lib.borrowBook(student, "Java");
		System.out.println("Books count: "+lib.getBookCount());
		System.out.println("show all books: "+lib.showBooks());
		
		
		
	}
}
