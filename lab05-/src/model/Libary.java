package model;

import java.util.ArrayList;

public class Libary {
	ArrayList<Book> books = new ArrayList<Book>();
	ArrayList<ReferencesBook> refBooks = new ArrayList<ReferencesBook>();
	ArrayList<Book> borrowedBook = new ArrayList<Book>();

	public void addBook(Book book){
		books.add(book);
	}
	
	public void addRefBook(ReferencesBook book){
		refBooks.add(book);
	}
	
	public void borrowBook(User student,String title){
		int index = -1;
		for(int i=0;i<books.size();i++){
			if(books.get(i).getTitle().equals(title)){
				index = i;
			}
		}	
			
		if(index!=-1){
			student.borrowBook(books.get(index));
			borrowedBook.add(books.get(index));
			books.remove(index);
		}
		
	}
	
	
	public int getBookCount(){
		return books.size();
	}
	
	public int getRefBookCount(){
		return refBooks.size();
	}
	
	public String showBooks(){
		String allBooks = "";
		for(Book book:books){
			allBooks += book.getTitle()+",";
		}
		return allBooks.substring(0,allBooks.length()-1);
	
	}
}
